# manifest_lge_geehrc

Copy this file to `.repo/local_manifests/local_manifest.xml` after `repo init`.
Then run `repo sync`.

To sync only files in the manifest:

```sh
repo sync "device/lge/geehrc" "kernel/lge/geehrc" "vendor/lge/geehrc"
```
